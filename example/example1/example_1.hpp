#pragma once

class example_1
{
private:
    /* data */
public:
    example_1() = default;
    virtual ~example_1() = default;
    void showCase();
};
