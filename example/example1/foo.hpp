#pragma once
class foo
{
private:
    /* data */
public:
    foo() = default;
    virtual ~foo() = default;
    void doFooStuff();
};
