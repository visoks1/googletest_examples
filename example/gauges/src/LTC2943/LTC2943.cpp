
#include "LTC2943.h"
#include "i2c.h"

namespace {
// registers
constexpr uint8_t REG_STATUS = 0x00;// 0x00 A Status R See Table 2
constexpr uint8_t REG_CONTROL = 0x01;// 0x01 B Control R / W3Ch

//addresses
constexpr uint8_t WRITE_ADDRESS = 0b1100'1000U;
constexpr uint8_t READ_ADDRESS = 0b1100'1001U;

constexpr uint8_t ADC_MODE_MASK = 0xC0U;
}// namespace

namespace gauge {
LTC2943::LTC2943(LTC2943Listener &listener)
  : m_listener(listener)
{
    // random thoughts (yea yea i know, comments like these should be in code review tool)
    // hmmm init here or not to init :D
    // same with deinit just on d-tor
    // also do we need to check if i2c is initialized before read/write? it should return false if it is not initialized right?
}

LTC2943::IsSuccess LTC2943::init()
{
    return i2cInit();
}

LTC2943::IsSuccess LTC2943::deinit()
{
    return i2cDeinit();
}

LTC2943::IsSuccess LTC2943::get_adc_mode(Adc_mode &mode)
{
    uint8_t data = 0;
    if (!read_single(REG_CONTROL, data)) {
        return false;
    }
    mode = static_cast<Adc_mode>(data & ADC_MODE_MASK);
    return true;
}

LTC2943::IsSuccess LTC2943::set_adc_mode(const Adc_mode mode)
{
    uint8_t data = 0U;
    if (!read_single(REG_CONTROL, data)) {
        return false;
    }
    data &= static_cast<uint8_t>(~ADC_MODE_MASK);// clear adc mode bits
    data |= static_cast<uint8_t>(static_cast<uint8_t>(mode) & ADC_MODE_MASK);// set new adc mode

    return write_single(REG_CONTROL, data);
}

LTC2943::IsSuccess LTC2943::check_alerts()
{
    uint8_t out_data = 0;
    if (!read_single(REG_STATUS, out_data)) {
        return false;
    }
    // A[7] Reserved
    // A[6] Current AlertIndicates one of the current limits was exceeded0
    // A[5] Accumulated Charge Overflow / UnderflowIndicates that the value of the ACR hit either top or bottom0
    // A[4] Temperature AlertIndicates one of the temperature limits was exceeded0
    // A[3] Charge Alert HighIndicates that the ACR value exceeded the charge threshold high limit0
    // A[2] Charge Alert LowIndicates that the ACR value exceeded the charge threshold low limit0
    // A[1] Voltage AlertIndicates one of the voltage limits was exceeded0
    // A[0] Undervoltage Lockout AlertIndicates recovery from undervoltage.If set to 1, a UVLO has occurred and the contents of the registers are uncertain
    constexpr uint8_t temp_warning_mask = 0b0001'0000U;
    constexpr uint8_t voltage_warning_mask = 0b0000'0010U;
    if ((out_data & temp_warning_mask) != 0U) {
        m_listener.on_temperature_alert();
    }
    if ((out_data & voltage_warning_mask) != 0U) {
        m_listener.on_voltage_alert();
    }
    return true;
}

LTC2943::IsSuccess LTC2943::write_single(const uint8_t reg, const uint8_t data)
{
    // tell which register we are interested in
    if (!i2cWrite(WRITE_ADDRESS, &reg, sizeof(reg))) {
        return false;
    }

    // tell what data we want it to contain
    if (!i2cWrite(WRITE_ADDRESS, &data, sizeof(data))) {
        return false;
    }

    return true;
}

LTC2943::IsSuccess LTC2943::read_single(const uint8_t reg, uint8_t &data)
{
    // tell which register we are interested in
    if (!i2cWrite(WRITE_ADDRESS, &reg, sizeof(reg))) {
        return false;
    }

    // ask to send it to us
    if (!i2cRead(READ_ADDRESS, &data, sizeof(data))) {
        return false;
    }

    return true;
}

}// namespace gauge
