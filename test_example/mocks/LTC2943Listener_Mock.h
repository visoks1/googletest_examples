#include "gmock/gmock.h"
#include "LTC2943Listener.h"


class LTC2943Listener_Mock final : public gauge::LTC2943Listener
{
  public:
    MOCK_METHOD(void, on_temperature_alert, (), (override)) {};
    MOCK_METHOD(void, on_voltage_alert, (), (override)) {};
};
