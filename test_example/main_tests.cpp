/**
 * Copyright (c) 2018 Akan Murat Cimen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"
#include "LTC2943.h"
#include "HAL_mocks/I2C_Mock.h"
#include "mocks/LTC2943Listener_Mock.h"

using ::testing::_;
using ::testing::Return;
using ::testing::Pointee;
using ::testing::Eq;
using ::testing::DoAll;
using ::testing::SetArgPointee;


class LTC2943_fixture : public ::testing::Test
{
  protected:
    void SetUp() override
    {
    }
    LTC2943Listener_Mock m_listener;
    I2C_Mock m_mock;

    static constexpr uint8_t WRITE_ADDRESS = 0b11001000;
    static constexpr uint8_t READ_ADDRESS = 0b11001001;
};


TEST_F(LTC2943_fixture, init_returns_i2c_init_status)
{
    gauge::LTC2943 battery_gauge(m_listener);

    EXPECT_CALL(m_mock, i2cInit).WillOnce(Return(true));
    EXPECT_TRUE(battery_gauge.init());
    EXPECT_CALL(m_mock, i2cInit).WillOnce(Return(false));
    EXPECT_FALSE(battery_gauge.init());
}

TEST_F(LTC2943_fixture, get_adc_mode_will_return_error_on_first_write)
{
    gauge::LTC2943 battery_gauge(m_listener);

    EXPECT_CALL(m_mock, i2cWrite(_, _, _)).WillOnce(Return(false));

    gauge::LTC2943::Adc_mode mode;
    EXPECT_FALSE(battery_gauge.get_adc_mode(mode));
}

TEST_F(LTC2943_fixture, get_adc_mode)
{
    gauge::LTC2943 battery_gauge(m_listener);
    {  // get AUTOMATIC mode
        EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(0x01)), 1))
          .WillOnce(Return(true));
        EXPECT_CALL(m_mock, i2cRead(READ_ADDRESS, _, _))
          .WillOnce(DoAll(SetArgPointee<1>(0xCA), Return(true)));

        gauge::LTC2943::Adc_mode mode;
        EXPECT_TRUE(battery_gauge.get_adc_mode(mode));
        EXPECT_EQ(gauge::LTC2943::Adc_mode::AUTOMATIC, mode);
    }
    {// get SLEEP mode
        EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(0x01)), 1))
          .WillOnce(Return(true));
        EXPECT_CALL(m_mock, i2cRead(READ_ADDRESS, _, _))
          .WillOnce(DoAll(SetArgPointee<1>(0x0A), Return(true)));

        gauge::LTC2943::Adc_mode mode;
        EXPECT_TRUE(battery_gauge.get_adc_mode(mode));
        EXPECT_EQ(gauge::LTC2943::Adc_mode::SLEEP, mode);
    }
}

TEST_F(LTC2943_fixture, set_adc_mode)
{
    gauge::LTC2943 battery_gauge(m_listener);

    const uint8_t value_before = 0x0A;
    const uint8_t value_after = 0xCA;

    EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(0x01)), 1))
      .Times(2)
      .WillRepeatedly(Return(true));
    EXPECT_CALL(m_mock, i2cRead(READ_ADDRESS, _, _))
      .WillOnce(DoAll(SetArgPointee<1>(value_before), Return(true)));

    EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(value_after)), 1))
      .WillOnce(Return(true));

    const auto mode = gauge::LTC2943::Adc_mode::AUTOMATIC;
    EXPECT_TRUE(battery_gauge.set_adc_mode(mode));
}

TEST_F(LTC2943_fixture, check_alerts)
{
    gauge::LTC2943 battery_gauge(m_listener);

    {// status 0xCA -> on_voltage_alert raised
        EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(0x00)), 1))
          .WillOnce(Return(true));
        EXPECT_CALL(m_mock, i2cRead(READ_ADDRESS, _, _))
          .WillOnce(DoAll(SetArgPointee<1>(0xCA), Return(true)));
        EXPECT_CALL(m_listener, on_voltage_alert());
        EXPECT_TRUE(battery_gauge.check_alerts());
    }
    {// status 0xFF -> all alerts raised
        EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(0x00)), 1))
          .WillOnce(Return(true));
        EXPECT_CALL(m_mock, i2cRead(READ_ADDRESS, _, _))
          .WillOnce(DoAll(SetArgPointee<1>(0xFF), Return(true)));
        EXPECT_CALL(m_listener, on_temperature_alert());
        EXPECT_CALL(m_listener, on_voltage_alert());
        EXPECT_TRUE(battery_gauge.check_alerts());
    }
    {// status 0x00 -> none alerts raised
        EXPECT_CALL(m_mock, i2cWrite(WRITE_ADDRESS, Pointee(Eq(0x00)), 1))
          .WillOnce(Return(true));
        EXPECT_CALL(m_mock, i2cRead(READ_ADDRESS, _, _))
          .WillOnce(DoAll(SetArgPointee<1>(0x00), Return(true)));
        EXPECT_CALL(m_listener, on_temperature_alert()).Times(0);
        EXPECT_CALL(m_listener, on_voltage_alert()).Times(0);
        EXPECT_TRUE(battery_gauge.check_alerts());
    }
}
