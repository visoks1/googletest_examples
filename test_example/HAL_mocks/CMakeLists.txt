add_library(HAL_mocks STATIC I2C_Mock.cpp)
target_include_directories(HAL_mocks 
    PUBLIC ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(
    HAL_mocks
    PUBLIC  gmock
            HAL
    PRIVATE project_options
            project_warnings 
    )
target_compile_definitions(HAL_mocks PUBLIC HAL_mocks)

