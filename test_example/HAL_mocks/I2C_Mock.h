#include "gmock/gmock.h"
#include "i2c.h"


class I2C_Mock final
{
  public:
    I2C_Mock();
    ~I2C_Mock();
    MOCK_CONST_METHOD0(i2cInit, bool());
    MOCK_CONST_METHOD0(i2cDeinit, bool());
    MOCK_CONST_METHOD0(i2cIsInitialized, bool());
    MOCK_CONST_METHOD3(i2cRead, bool(uint8_t address, uint8_t *pDst, uint16_t dataSize));
    MOCK_CONST_METHOD3(i2cWrite, bool(uint8_t address, const uint8_t *pSrc, uint16_t dataSize));
};
